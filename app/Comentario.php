<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{

  protected $table = "comentarios";

	protected $fillable = [
    'nombre',
    'tipo_user',
    'comentario',
    'id_peticion',
	];
	
	/**
	* Obtiene la peticion.
	*/
	public function peticion()
	{
		return $this->belongsTo('App\Peticion', 'id_peticion');
	}
}
