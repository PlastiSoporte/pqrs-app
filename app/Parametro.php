<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametro extends Model
{
  protected $table = "parametrizacion";

	protected $fillable = [
     'dias_peticion',
     'dias_queja',
     'dias_reclamo',
     'dias_sugerencia',
	];
}
