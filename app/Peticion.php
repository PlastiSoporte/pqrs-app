<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peticion extends Model
{
	protected $table = "peticiones";

	protected $fillable = [
      'nombre',
      'tipo_documento',
      'documento',
      'telefono',
      'email',
      'tipo',
      'peticion',
      'respuesta',
      'id_user_asignado',
      'id_estado_interno',
      'id_estado_externo',
	];

	/**
	* Obtiene el usuario asignado.
	*/
	public function user()
	{
		return $this->belongsTo('App\User', 'id_user_asignado');
	}
	/**
	* Obtiene el estodo interno.
	*/
	public function estadoIN()
	{
		return $this->belongsTo('App\Estado', 'id_estado_interno');
	}
	/**
	* Obtiene el estado externo.
	*/
	public function estadoEX()
	{
		return $this->belongsTo('App\Estado', 'id_estado_externo');
	}
}
