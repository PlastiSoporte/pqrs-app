<?php

namespace App\Http\Controllers;

use App\Peticion;
use App\Parametro;
use App\Estado;
use App\User;
use App\Comentario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreatePeticionRequest;
use App\Http\Requests\UpdatePeticionRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnvioPeticion;
use App\Mail\EnvioRespuesta;

class PeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hoy = date_create();
        $pa = Parametro::find(1);
        $pes = Peticion::all();
        foreach ($pes as $p) {
            if ($p->respuesta == null or $p->respuesta == '') {
                switch ($p->tipo) {
                    case 'Petición':
                        $p->created_at = date_create($p->created_at);
                        $interval = date_diff($p->created_at, $hoy); 
                        $dias = $interval->format('%a');
                        if ($dias > $pa->dias_peticion) {
                            $p->id_estado_interno = 2;
                            $p->update();   
                        }
                        break;
                    case 'Queja':
                        $p->created_at = date_create($p->created_at);
                        $interval = date_diff($p->created_at, $hoy); 
                        $dias = $interval->format('%a');
                        if ($dias > $pa->dias_queja) {
                            $p->id_estado_interno = 2;
                            $p->update();   
                        }
                        break;
                    case 'Reclamo':
                        $p->created_at = date_create($p->created_at);
                        $interval = date_diff($p->created_at, $hoy); 
                        $dias = $interval->format('%a');
                        if ($dias > $pa->dias_reclamo) {
                            $p->id_estado_interno = 2;
                            $p->update();   
                        }
                        break;
                    default:
                        $p->created_at = date_create($p->created_at);
                        $interval = date_diff($p->created_at, $hoy); 
                        $dias = $interval->format('%a');
                        if ($dias > $pa->dias_sugerencia) {
                            $p->id_estado_interno = 2;
                            $p->update();   
                        }
                        break;
                }
            }
        }

        $peticiones = Peticion::paginate(20);
        return view('peticion.index', ['activo' => 'peticiones', 'peticiones' => $peticiones, 'dias_peticion' => $pa->dias_peticion, 'dias_queja' => $pa->dias_queja, 'dias_reclamo' => $pa->dias_reclamo, 'dias_sugerencia' => $pa->dias_sugerencia]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peticion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePeticionRequest $request)
    {
        $estados = Estado::all();
        $ex = 0;
        foreach ($estados as $e) {
            if ($e->estado == 'Pendiente') {
                $ex = $e->id;
            }
        }
        $p = Peticion::create([
            'nombre' => $request->input('nombre'),
            'tipo_documento' => $request->input('tipo_documento'),
            'documento' => $request->input('documento'),
            'telefono' => $request->input('telefono'),
            'email' => $request->input('email'),
            'tipo' => $request->input('tipo'),
            'peticion' => $request->input('peticion'),
            'id_user_asignado' => 0,
            'id_estado_interno' => 1,
            'id_estado_externo' => $ex,
        ]);

        //envio correos a todos los administradores del pqrs
        $users = User::all();
        foreach ($users as $user) {
            if ($user->tipo == 'admin') {
                Mail::to($user->email)->send(new EnvioPeticion($p));
            }            
        }


        //dd('hola');
        return redirect('peticion/create')->with('status', '¡Tu peticion ha sido registrada!. Este es tu numero de radical: '.$p->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function varificacion()
    {
        return view('peticion.verificacion');
    }

     public function validacion(Request $request)
    {
        $p = Peticion::find($request->radical);
        if (!is_null($p)) {
            if ($p->email == $request->email) {
            $users = User::all();
            $comentarios = Comentario::where('id_peticion', $p->id)->orderBy('id', 'des')
            ->get();
            return view('peticion.estado', ['activo' => 'peticiones', 'p' => $p, 'users' => $users, 'comentarios' => $comentarios]);
            }else{
                return redirect('peticion/verificacion')->with('status', 'Correo electronico incorrecto.');
            }
        }else{
            return redirect('peticion/verificacion')->with('status', 'El numero de radical no existe.');
        }
        //return view('peticion.verificacion');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $p = Peticion::find($id);
        $users = User::all();
        $comentarios = Comentario::where('id_peticion', $id)->orderBy('id', 'des')
            ->get();
        return view('peticion.edit', ['activo' => 'peticiones', 'p' => $p, 'users' => $users, 'comentarios' => $comentarios]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePeticionRequest $request, $id)
    {
        $p = Peticion::find($id);
        $estadoIN = $p->id_estado_interno;
        $estadoEX = $p->id_estado_externo;
        if ($request->respuesta) {
            $estadoIN = 4;
            $estadoEX = 4;
        }

        if ($request->respuesta <> null or $request->respuesta <> '') {
            if ($p->respuesta == null or $p->respuesta == '') {
                Mail::to($p->email)->send(new EnvioRespuesta($p, $request->respuesta));
            }
        }
        $p->respuesta = $request->respuesta;
        $p->id_user_asignado = $request->id_user_asignado;
        $p->id_estado_interno = $estadoIN;
        $p->id_estado_externo = $estadoEX;
        $p->update();
       return redirect('peticion')->with('status', '¡Peticion actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
