<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class PerfilController extends Controller
{
    
    public function edit($id)
    {
       $user = User::find($id);
        return view('perfil.edit', ['activo' => '', 'user' => $user ,]);           
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(request $request, $id)
    {
         $user = User::find($id);
         $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
         ]);
         return view('perfil.edit', ['activo' => '', 'user' => $user, 'status' => 'Perfil actualizado!', ]);
    }

    public function updatepass(Request $request)
    {
        $c = 'No';
        $user = User::find($request->id);
        if (Hash::check($request->actual_pass, Auth::user()->password)) {
            $h = bcrypt($request->nueva_pass); //encripto la nueva contraseña
            $user->password = $h;
            $user->update();
            $c = 'Si';
        }
        return $c; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
