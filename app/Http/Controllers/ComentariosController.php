<?php

namespace App\Http\Controllers;

use App\Comentario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComentariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd('hola');
        $c = Comentario::create([
            'nombre' => $request->nombre,
            'tipo_user' => $request->tipo_user,
            'comentario' => $request->comentario,
            'id_peticion' => $request->id_peticion,
        ]);

        $date = date_create($c->created_at);
        $date = date_format($date, 'd/m/Y');

       $dato = [
            'nombre' => $c->nombre,
            'fecha' => $date,
            'comentario' => $c->comentario,
        ];

        return $dato;
    }

    public function storecliente(Request $request)
    {
        //dd('hola');
        $c = Comentario::create([
            'nombre' => $request->nombre,
            'tipo_user' => $request->tipo_user,
            'comentario' => $request->comentario,
            'id_peticion' => $request->id_peticion,
        ]);

        $date = date_create($c->created_at);
        $date = date_format($date, 'd/m/Y');

       $dato = [
            'nombre' => $c->nombre,
            'fecha' => $date,
            'comentario' => $c->comentario,
        ];

        return $dato;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
