<?php

namespace App\Http\Middleware;

use Closure;
use App\Peticion;

class peticionexterna
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!is_null($request->radical)) {
            return $next($request);  
        }else{
            return redirect('peticion/create');
        }   
    }
}
