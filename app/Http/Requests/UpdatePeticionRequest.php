<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePeticionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'respuesta'=> 'max:500',
        ];
    }

    public function messages()
    {
        return [
            'respuesta.max' => 'Tu respuesta no debe superar los 500 caracteres',
        ];
    }
}
