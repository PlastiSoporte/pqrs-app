<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:150',
			'email' => 'required|email|max:100|unique:users,email,'.$this->id,
			'password' => 'max:30|confirmed',
			'tipo' => 'required',
		];
	}

	public function messages()
	{
		return [
			'name.required' => 'Ingresa el nombre',
			'name.max' => 'El nombre no debe superar los 150 caracteres',
			'email.required' => 'Ingresa el e-mail',
			'email.email' => 'Ingresa un email válido',
			'email.max' => 'El e-mail no debe superar los 100 caracteres',
			'email.unique' => 'El e-mail ya está registrado',
			'password.max' => 'La contraseña debe tener menos de 30 caracteres',
			'password.confirmed' => 'Deben de coincidir las contraseñas',
			'tipo.required' => 'Selecciona el tipo de usuario',
		];
	}
}
