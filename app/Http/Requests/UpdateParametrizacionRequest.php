<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateParametrizacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dias_peticion' => 'required|max:2',
            'dias_queja' => 'required|max:2',
            'dias_reclamo' => 'required|max:2',
            'dias_sugerencia' => 'required|max:2',
        ];
    }

    public function messages()
    {
        return [
            'dias_peticion.required' => 'Ingresa el numero de dias para la petición',
            'dias_peticion.max' => 'Los dias de la peticion no debe superar los 150 caracteres',
            'dias_queja.required' => 'Ingresa el numero de dias para la petición',
            'dias_queja.max' => 'Los dias de la peticion no debe superar los 150 caracteres',
            'dias_reclamo.required' => 'Ingresa el numero de dias para la petición',
            'dias_reclamo.max' => 'Los dias de la peticion no debe superar los 150 caracteres',
            'dias_sugerencia.required' => 'Ingresa el numero de dias para la petición',
            'dias_sugerencia.max' => 'Los dias de la peticion no debe superar los 150 caracteres',
        ];
    }
}
