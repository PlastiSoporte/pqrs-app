<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePeticionRequest extends FormRequest
{
   /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'nombre'=> 'required|max:150',
          'tipo_documento'=> 'required',
          'documento'=> 'required|max:30',
          'telefono'=> 'max:12',
          'email'=> 'required|max:100|email',
          'tipo'=> 'required',
          'peticion'=> 'required|max:500',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Ingresa el nombre',
            'nombre.max' => 'El nombre no debe superar los 150 caracteres',
            'tipo_documento.required' => 'Selecciona un tipo de documento',
            'documento.required' => 'Ingresa el numero de documento',
            'documento.max' => 'El numero de documento no debe superar los 30 caracteres',
            'telefono.max' => 'El numero de telefono no debe superar los 12 caracteres',
            'email.required' => 'Ingresa un correo electronico',
            'email.max' => 'El correo electronico no debe superar los 100 caracteres',
            'email.email' => 'Ingresa un correo electronico válido',
            'tipo.required' => 'Selecciona un tipo de petición',
            'peticion.required' => 'Ingresa tu petición',            
            'peticion.max' => 'Tu petición no debe superar los 500 caracteres',
        ];
    }
}
