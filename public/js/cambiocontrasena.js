function editarPassword(nueva, actual, user) {
    url = $('input[name=rutapassword]').val();
    token = $('input[name=_token]').val();
  // // Hago la llamada ajax
  $.post(url, {
      '_token': token,
      'id': user,
      'nueva_pass': nueva,
      'actual_pass': actual,
    }, function(dato) {
        if (dato == 'Si') {
          $('#alert-success-contraseña').css('display','block');
          $('#nueva_contraseña').val('');
          $('#confirmar_contraseña').val('');
          $('#contraseña_acual').val('');
        }else{
          $('#alert-error-contraseña').css('display','block');
          $('#span_contraseña_actual').text('Contraseña Incorrecta');
          $('#div-contraseña-actual').addClass('has-error');
        }
      });
}
$('#cambiar').click(function() {
  $('#alert-success-contraseña').css('display','none');
  $('#alert-error-contraseña').css('display','none');
     // validacion de la primera contraseña
			if ($('#nueva_contraseña').val() == '') {
            $('#span_nueva_contraseña').text('Ingresa una nueva contraseña');
            $('#div-nueva-contraseña').addClass('has-error');
            validation1 = 0;
      }else{
        if ($('#nueva_contraseña').val().length > 5) {
          if ($('#nueva_contraseña').val().length > 30) {
            $('#span_nueva_contraseña').text('La contraseña no debe sobrepasar los 30 caracteres');
            $('#div-nueva-contraseña').addClass('has-error');
            validation1 = 0;
          }else{
            $('#span_nueva_contraseña').text('');
            $('#div-nueva-contraseña').removeClass('has-error');
            validation1 = 1;
          }
        }else{
          $('#span_nueva_contraseña').text('La contraseña debe tener minomo 6 caracteres');
          $('#div-nueva-contraseña').addClass('has-error');
          validation1 = 0;
        }
      }
      // validacion de la segunda contraseña
      if ($('#confirmar_contraseña').val() == '') {
            $('#span_confirmar_contraseña').text('Confirma la contraseña');
            $('#div-confirmar-contraseña').addClass('has-error');
            validation2 = 0;
      }else{
        if ($('#confirmar_contraseña').val() == $('#nueva_contraseña').val() ) {
            $('#span_confirmar_contraseña').text('');
            $('#div-confirmar-contraseña').removeClass('has-error');
            validation2 = 1;
          }else{
            $('#span_confirmar_contraseña').text('Las contraseñas no coinciden');
            $('#div-confirmar-contraseña').addClass('has-error');
             validation2 = 0;                
          }
        }
      // validacion de la contraseña actual
      if ($('#contraseña_acual').val() == '') {
          $('#span_contraseña_actual').text('Ingresa la contraseña');
          $('#div-contraseña-actual').addClass('has-error');
          validation3 = 0;
      }else{
          $('#span_contraseña_actual').text('');
          $('#div-contraseña-actual').removeClass('has-error');
          validation3 = 1;
      }  
          validation = validation1+validation2+validation3;
          if (validation == 3) {
            editarPassword($('#nueva_contraseña').val(), $('#contraseña_acual').val(), $('#id_user').val());
            $('#alert-success-contraseña').css('display','none');
            $('#alert-error-contraseña').css('display','none');
          }
        });