function crearcometario(comentario, nombre, tipo_user, id_peticion) {	
	url = $('input[name=crearcomentario]').val();
  token = $('input[name=_token]').val();
  var f;
  if (tipo_user == 'cliente') {
    f = 'user.png';
  }else{
    f = 'logo.png';
  }
  var c;
  //Hago la llamada ajax
  $.post(url, {
      '_token': token,
      'nombre': nombre,
      'comentario': comentario,
      'tipo_user': tipo_user,
      'id_peticion': id_peticion,
    }, function(dato) { //LO QUE ME DEVUELVA LA CONSULTA LA PEGO EN LA LISTA DE COMENTARIOS
      c = '<div class="box-comment"><img class="img-circle img-sm" src="http://localhost/PQRS_app/public/img/'+f+'" alt="User Image"><div class="comment-text"><span class="username">'+dato['nombre']+'<span class="text-muted pull-right">'+dato['fecha']+'</span></span>'+dato['comentario']+'</div></div>';
      $('#lista-comentarios').prepend(c);
     $('#comentario').val('');
  });
}

$('#añadir-comentario').click(function() {

    if ($('#comentario').val() == '') {
      $('#div-comentario').addClass('has-error');
      $('#span-comentario').text('Ingresa un comentario.');
      validation = 0;
    }else{
      $('#div-comentario').removeClass('has-error');
      $('#span-comentario').text('');
      validation = 1;
    }

    if (validation == 1) {
      crearcometario($('#comentario').val(), $('#nombre-user-comentario').val(), $('#tipo-comentario').val(), $('#id_peticion').val());
    }

  });