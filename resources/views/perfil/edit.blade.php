@extends('template.app')

@section('htmlheader_title', 'Perfil usuario')

@section('contentheader_title', 'Usuario')
<style>	.red {color: #dd4b39 !important;} </style>

@section('contentheader_description', $user->name)

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li class="active">Perfil</li>
@endsection

@section('main-content')

	
	<form method="POST" action="{{ route('perfil.update', $user->id) }}">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		<input type="hidden" value="{{ route('perfil.updatepass', $user->id) }}" name="rutapassword">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				 {{-- Alerta de cambio --}}
				  @if(isset($status))
				  	@if($status <> 'nada')
					    <div class="alert alert-success alert-dismissible" role="alert">
					      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					      {{ $status }}
					    @php
					    $status = 'nada';	
					    @endphp
					    </div>
						@endif
				  @endif
				<div class="box box-primary">
					
					<div class="box-header with-border">
						<h3 class="box-title">Información del usuario</h3>
					</div>
		
					<div class="box-body">

						<img class="profile-user-img img-responsive img-circle" src="{{ Gravatar::get($user->email) }}">
						
						@php $campo = 'name' @endphp
						<div class="form-group @if($errors->has($campo)) has-error @endif">
							<label for="{{ $campo }}">Nombre</label>
							<input type="text" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{ old($campo, $user->name) }}">
							@if ($errors->has($campo))
								@foreach ($errors->get($campo) as $error)
									<span class="help-block">{{ $error }}</span>
								@endforeach
							@endif
						</div>
						
						@php $campo = 'email' @endphp
						<div class="form-group @if($errors->has($campo)) has-error @endif">
							<label for="{{ $campo }}">Email</label>
							<input type="email" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="ejemplo@gmail.com" value="{{ old('email', $user->email) }}" readonly>
							@if ($errors->has($campo))
								@foreach ($errors->get($campo) as $error)
									<span class="help-block">{{ $error }}</span>
								@endforeach
							@endif
						</div>

						@if(Auth::user()->tipo == 'conduc')


							@php $campo = 'estado' @endphp
							<div class="form-group">
								<label>Estado</label>
								<input type="text" class="form-control" value="{{ $conductor->estado->$campo }}" readonly>
							</div>

							@php $campo = 'saldo' @endphp
							<div class="form-group">
								<label>Saldo</label>
								@if($conductor->$campo < 0)
										<input type="number" class="form-control"value="{{ $conductor->$campo * (-1) }}" readonly>
										<span class="text-info">El saldo esta en contra</span>
								@else
								  	<input type="number" class="form-control"value="{{ $conductor->$campo}}" readonly>
										<span class="text-info">El saldo esta a favor</span>
								@endif
							</div>
						@endif

						<div class="form-group">
							<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-default2">
                  Cambiar contraseña
     						</button>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>

		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="box box-primary">
					<div class="box-body text-center">
						<a href="{{ URL::previous() }}" role="button" class="btn btn-default">Cancelar</a>
						<button type="submit" class="btn btn-info">Actualizar</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>

@endsection
<!-- modal de editar costo -->
  <div class="modal fade" id="modal-default2">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Cambiar contraseña</h4>
        </div>

        <div class="modal-body"> 

				<div class="alert alert-success alert-dismissible" role="alert" id="alert-success-contraseña" style="display: none;">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      Cambio Exitoso!
			    </div>
			    <div class="alert alert-danger alert-dismissible" role="alert" id="alert-error-contraseña" style="display: none;">
			      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			      <p id="mensaje_error">
			      	Contraseña incorrecta
			      </p>
			    </div>    
			
			<input type="hidden" value="{{ $user->id }}" id="id_user">

			@php $campo = 'nueva_contraseña' @endphp
			<div class="form-group @if($errors->has($campo)) has-error @endif" id="div-nueva-contraseña" >
				<label for="{{ $campo }}">Nueva contraseña</label>
				<input type="password" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa una nueva contraseña" value="{{ old($campo) }}" min="0">
				<span class="text-info red" id="span_nueva_contraseña">
				</span>
			</div>

			@php $campo = 'confirmar_contraseña' @endphp
			<div class="form-group @if($errors->has($campo)) has-error @endif" id="div-confirmar-contraseña" >
				<label for="{{ $campo }}">Confirmar contraseña</label>
				<input type="password" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Confirma la contraseña" value="{{ old($campo) }}" min="0">
				<span class="text-info red" id="span_confirmar_contraseña">
				</span>
			</div>

			@php $campo = 'contraseña_acual' @endphp
			<div class="form-group @if($errors->has($campo)) has-error @endif" id="div-contraseña-actual" >
				<label for="{{ $campo }}">Ingresa tu contraseña actual para realizar los cambios</label>
				<input type="password" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa la contraseña actual" value="{{ old($campo) }}" min="0">
				<span class="text-info red" id="span_contraseña_actual">
				</span>
			</div>
			

      </div>
            <div class="modal-footer">
              <a  class="btn btn-default pull-left" data-dismiss="modal">Cancelar</a>
              <a type="submit" class="btn btn-info" id="cambiar">Cambiar</a>
            </div>
      </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
<!-- /.modal -->

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/js/cambiocontrasena.js') }}" type="text/javascript"></script>
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
		$(function () {
			//Initialize Select2 Elements
			$('.select2').select2();
		})


	</script>
@endsection