@extends('template.app')

@section('htmlheader_title', 'Editar usuario')

@section('stylesheet')
	<link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
@endsection

@section('contentheader_title', 'Editar usuario')

@section('contentheader_description', $user->name)

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li><a href="{{ url('users') }}">Usuarios</a></li>
	<li class="active">Editar usuario</li>
@endsection

@section('main-content')
	
	<form method="POST" action="{{ route('users.update', $user->id) }}">
		{{ method_field('PUT') }}
		{{ csrf_field() }}
		  <div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Información del usuario</h3>
          </div>
    
          <div class="box-body">
            
            @php $campo = 'name' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Nombre</label>
              <input type="text" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{ old($campo, $user->name) }}" @if(Auth::user()->tipo <> 'superuser' and $user->tipo == 'superuser') disabled @endif>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>
            
            @php $campo = 'email' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Email</label>
              <input type="email" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="ejemplo@gmail.com" value="{{ old('email', $user->email) }}" @if(Auth::user()->tipo <> 'superuser' and $user->tipo == 'superuser') disabled @endif>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

            @php $campo = 'password' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Contraseña</label>
              <input type="password" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="******" @if(Auth::user()->tipo <> 'superuser' and $user->tipo == 'superuser') disabled @endif>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

            @php $campo = 'password_confirmation' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Confirmar contraseña</label>
              <input type="password" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="******" @if(Auth::user()->tipo <> 'superuser' and $user->tipo == 'superuser') disabled @endif>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

            @php $campo = 'tipo' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Tipo de usuario</label>
              <select class="form-control select2" name="{{ $campo }}" id="{{ $campo }}" @if(Auth::user()->tipo <> 'superuser' and $user->tipo == 'superuser') disabled @endif>
                <option value="">Seleccionar..</option>
                <option value="superuser" @if(old($campo, $user->tipo) == 'superuser') selected="selected" @endif >Super usuario</option>
                <option value="admin" @if(old($campo, $user->tipo) == 'admin') selected="selected" @endif>Administrador</option>
                <option value="user" @if(old($campo, $user->tipo) == 'user') selected="selected" @endif>Usuario</option>
              </select>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body text-center">
						<a href="{{ url('users') }}" role="button" class="btn btn-default">Cancelar</a>
						<button type="submit" class="btn btn-info">Actualizar</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>

@endsection

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
		$(function () {
			//Initialize Select2 Elements
			$('.select2').select2();

      //iCheck for checkbox and radio inputs
        $('input[type="radio"].flat-orange').iCheck({
          radioClass : 'iradio_flat-orange'
        })
		})
	</script>
@endsection