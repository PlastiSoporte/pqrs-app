@extends('template.app')

@section('htmlheader_title', 'Usuarios')

@section('contentheader_title', 'Usuarios')

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li class="active">Usuarios</li>
@endsection

@section('main-content')
	
	{{-- Barra de botones --}}
	<nav class="navbar navbar-default">
		<div class="container-fluid text-right">
			<a type="button" class="btn btn-primary navbar-btn" href="{{ route('users.create') }}">Nuevo usuario</a>
		</div>
	</nav>

	{{-- Alerta de acciones --}}
	@if (session('status'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ session('status') }}
		</div>
	@endif

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Usuarios registrados</h3>
					<div class="box-tools col-md-2">
						<form method="GET" action="{{ route('users.index') }}">
						<div class="input-group input-group-sm" >
							<input type="text" name="filtro" class="form-control pull-right" placeholder="Buscar" id="buscar">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
						</form>
					</div>
				</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
					<table class="table table-hover" id="tabla">
						<tr>
							<th>Nombre</th>
							<th>E-mail</th>
							<th>Tipo de usuario</th>
							<th class="text-center">Detalles</th>
						</tr>

						@foreach ($users as $user)
							<tr>
								<td><a href="{{ route('users.edit', $user->id) }}">{{ $user->name }}</a></td>
								<td>{{ $user->email }}</td>
								<td>@if($user->tipo == 'superuser') Super usuario 
											@elseif($user->tipo == 'admin') Administrador  
											@elseif($user->tipo == 'user')  Usuario 
									  @endif
								</td>
								<td class="text-center">
									<a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">Detalles</a>
								</td>
							</tr>
						@endforeach
					</table>
					
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
      <div class="col-md-12 text-center">
				{{ $users->links() }}
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	{{-- <script src="{{ asset('/js/buscador.js') }}" type="text/javascript"></script> --}}
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>
@endsection