@extends('template.app')

@section('htmlheader_title', 'Parametrización')

@section('stylesheet')
	{{-- <link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet"> --}}
@endsection

@section('contentheader_title', 'Parametrización')

@section('contentheader_description', 'Dias calendario')

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li class="active">Parametrización</li>
@endsection

@section('main-content')

		  <div class="row">
			<div class="col-md-8 col-md-offset-2">

        {{-- Alerta de acciones --}}
        @if (session('status'))
          <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session('status') }}
          </div>
        @endif
				
        <div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Dias calendario de respuesta</h3>
          </div>
    
          <div class="box-body">
       
            @php $campo = 'dias_peticion' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Dias para petición</label>
              <input type="text" disabled class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{$parametro->$campo}}" readonly>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>
            
            @php $campo = 'dias_queja' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Dias para queja</label>
              <input type="text" disabled class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{$parametro->$campo}}" readonly>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

              @php $campo = 'dias_reclamo' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Dias para el reclamo</label>
              <input type="text" disabled class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{$parametro->$campo}}" readonly>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

              @php $campo = 'dias_sugerencia' @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Dias para la sugerencia</label>
              <input type="text" disabled class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{$parametro->$campo}}" readonly>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->

		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-body text-center">
						<a href="{{ URL::previous() }}" role="button" class="btn btn-default">Cancelar</a>
						<a href="{{route('parametrizacion.edit', $parametro->id)}}" class="btn btn-info">Actualizar</a>
					</div>
				</div>
			</div>
		</div>

@endsection

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	{{-- <script src="{{ asset('/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script> --}}
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
		$(function () {
		//nada
		})
	</script>
@endsection