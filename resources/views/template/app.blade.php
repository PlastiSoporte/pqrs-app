<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

@section('htmlheader')
    @include('template.partials.htmlheader')
@show


<body class="skin-yellow sidebar-mini">
<div id="app">
    <div class="wrapper">

    @include('template.partials.mainheader')

    @include('template.partials.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @include('template.partials.contentheader')

        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    @include('template.partials.controlsidebar')

    @include('template.partials.footer')

</div><!-- ./wrapper -->
</div>
@yield('scripts')


</body>
</html>
