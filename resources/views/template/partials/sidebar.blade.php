<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		@if (! Auth::guest())
			<div class="user-panel">
				<div class="pull-left image">
					<img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
				</div>
				<div class="pull-left info">
					<p>{{ Auth::user()->name }}</p>
				</div>
			</div>
		@endif


		<!-- Sidebar Menu -->
		<ul class="sidebar-menu">
			<li class="header">Menú principal</li>
			<!-- Optionally, you can add icons to the links -->

			<li @if($activo == 'home') class="active" @endif >
				<a href="{{ url('/') }}"><i class='fa fa-home'></i> <span>Inicio</span></a>
			</li>

			@if(Auth::user()->tipo == 'admin' or Auth::user()->tipo == 'superuser')
			<li @if($activo == 'parametros') class="active" @endif>
				<a href="{{ url('/parametrizacion') }}"><i class='fa fa-check-square-o'></i> <span>Parametrización</span></a>
			</li>

			<li @if($activo == 'users') class="active" @endif>
				<a href="{{ url('/users') }}"><i class='fa fa-users'></i> <span>Usuarios</span></a>
			</li>
			@endif
			
			<li @if($activo == 'peticiones') class="active" @endif>
				<a href="{{ url('/peticion') }}"><i class='fa fa-list'></i> <span>Peticiones</span></a>
			</li>

		</ul>
		
		<!-- /.sidebar-menu -->

	</section>
	<!-- /.sidebar -->
</aside>
