<!-- Main Footer -->
<footer class="main-footer hidden-print" style="color: #d4d4d4; background: #222d32; !important;">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <strong>Desarrollado por: </strong><a href="http://plastimedia.com"><img src="{{ asset('/img/marca.png') }}" alt="Plastimedia Studio"></a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="http://ccmtelevision.tv" target="_blank">Canal Ccm - Corporacion PROACODEMA. Todos los derechos reservados</a></strong> 
</footer>