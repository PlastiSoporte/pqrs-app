@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    PQRS CCM
@endsection
@section('content')

<body class="hold-transition login-page" >

    <div id="app">      
        <div class="col-md-10 col-md-offset-1">   
        <br>
            <div>
                <img src="{{ asset('/img/logo.png') }}" alt="CCM-tv">
            </div>
        <br>

        
        <input type="hidden" name="crearcomentario" value="{{ route('comentario.cliente') }}">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-xs-2">
                <a href="{{url('peticion/create')}}" class="btn btn-primary btn-block btn-flat">Ir al inicio</a>
            </div><!-- /.col -->
        </div>
        <br>

        <div class="col-md-6">
        
            <div class="box box-warning">
                <div class="box-header with-border">
                    <br>
                    <h3 class="box-title">Información personal</h3>
                </div>

            <div class="box-body">

                @php $campo = 'nombre'; @endphp     
                <div class="form-group">
                    <label for="{{ $campo }}">Nombre</label>
                    <input type="text" class="form-control" value="{{ $p->$campo }}" disabled>
                </div>

                @php $campo = 'tipo_documento'; @endphp
                <div class="form-group">
                    <label for="{{ $campo }}">Tipo de documento</label>
                    <select class="form-control select2" disabled>
                    <option value="">Seleccionar..</option>
                    <option value="Tarjeta de identidad" @if($p->$campo == 'Tarjeta de identidad') selected="selected" @endif >Tarjeta de identidad</option>

                    <option value="Cédula de Ciudadanía" @if($p->$campo == 'Cédula de Ciudadanía') selected="selected" @endif >Cédula de Ciudadanía</option>

                    <option value="Registro Civil" @if($p->$campo == 'Registro Civil') selected="selected" @endif >Registro Civil</option>

                    <option value="Cédula de Extranjería" @if($p->$campo == 'Cédula de Extranjería') selected="selected" @endif >Cédula de Extranjería</option>

                    <option value="Pasaporte" @if($p->$campo == 'Pasaporte') selected="selected" @endif >Pasaporte</option>

                    <option value="Nit" @if($p->$campo == 'Nit') selected="selected" @endif >Nit</option>
                    </select>
                </div>

                @php $campo = 'documento'; @endphp
                <div class="form-group">
                    <label>Numero de documento</label>
                    <input type="number" class="form-control" value="{{$p->$campo}}" disabled>
                </div>

                @php $campo = 'telefono'; @endphp
                <div class="form-group">
                    <label>Numero de telefono</label>
                    <input type="number" class="form-control" value="{{$p->$campo}}" disabled>
                </div>

                @php $campo = 'email'; @endphp
                <div class="form-group">
                    <label for="{{ $campo }}">Email</label>
                    <input type="text" class="form-control" value="{{ $p->$campo }}" disabled>
                </div>

                @php $campo = 'peticion'; @endphp
                <div class="form-group">
                    <label for="{{ $campo }}">Descripción</label>
                    <textarea class="form-control" cols="20" rows="10" readonly>{{ $p->$campo }}</textarea>
                </div>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-6">
            <div class="box box-warning">

                <div class="box-header with-border">
                    <h3 class="box-title">Respuesta</h3>
                </div>

                <div class="box-body">

                @php $campo = 'respuesta'; @endphp
                <div class="form-group">
                    <label for="{{ $campo }}">Estado de la petición: </label>
                    <span class="label bg-orange">
                        {{ $p->estadoEX->estado }}      
                    </span>
                </div>

                @php $campo = 'respuesta'; @endphp
                <div class="form-group">
                    <label for="{{ $campo }}">Respuesta a la petición</label>
                    <textarea class="form-control" cols="20" rows="10" maxlength="500" name="{{$campo}}" placeholder="Sin respuesta" readonly>{{ $p->$campo }}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                </div>
                <!-- .box -->

                <div class="box box-warning">

                    <div class="box-header with-border">
                        <h3 class="box-title">Comentarios</h3>
                    </div>

                <div class="box-body">
                    @php $campo = 'comentario'; @endphp
                    <div class="form-group" id="div-{{$campo}}">
                        <label for="{{ $campo }}">Hacer un comentario</label>
                        <textarea class="form-control" placeholder="Comentario.." id="{{$campo}}"></textarea>
                        <input type="hidden" id="nombre-user-{{$campo}}" value="{{$p->nombre}}">
                        <input type="hidden" id="tipo-{{$campo}}" value="cliente">
                        <input type="hidden" id="id_peticion" value="{{ $p->id }}">
                        <span class="help-block" id="span-{{$campo}}"></span>
                    </div>
                    <a class="btn btn-info" id="añadir-comentario">Comentar</a>
                    <hr>

                    <div class="box-footer box-comments" id="lista-comentarios">
                    @foreach($comentarios as $c)
                            @php
                            if($c->tipo_user == 'cliente'){
                                $foto = asset('/img/user.png');
                            }else{
                                $foto = asset('/img/logo.png'); 
                            }
                                $date = date_create($c->created_at);
                                $date = date_format($date, 'd/m/Y');
                            @endphp
                         <div class="box-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="{{ $foto }}" alt="User Image">
                            <div class="comment-text">
                            <span class="username">
                                {{ $c->nombre }}
                                <span class="text-muted pull-right">{{ $date }}</span>
                            </span>
                            {{ $c->comentario }}
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- .box -->
        </div>
        <!-- /.col -->

    </div>

  </div>
  {{-- /div-app --}}

    @include('adminlte::layouts.partials.scripts_auth')
    <script src="{{ asset('/js/crearcomentario.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
