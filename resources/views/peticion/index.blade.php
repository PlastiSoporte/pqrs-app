@extends('template.app')

@section('htmlheader_title', 'Peticiones')

@section('contentheader_title', 'Peticiones')

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li class="active">Peticiones</li>
@endsection

@section('main-content')
	
	{{-- Alerta de acciones --}}
	@if (session('status'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ session('status') }}
		</div>
	@endif

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Peticiones</h3>
					{{-- <div class="box-tools col-md-2">
						<form method="GET" action="{{ route('users.index') }}">
						<div class="input-group input-group-sm" >
							<input type="text" name="filtro" class="form-control pull-right" placeholder="Buscar" id="buscar">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
						</form>
					</div> --}}
				</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
					<table class="table table-hover" id="tabla">
						<tr>
							<th class="text-center">Numero de remisión</th>
							<th class="text-center">Nombre</th>
							<th class="text-center">Tipo de Petición</th>
							<th class="text-center">Fecha de Petición</th>
							<th class="text-center">Estado de petición</th>
							<th class="text-center">Detalles</th>
						</tr>
						@php
							$hoy = date_create();
						@endphp

						@foreach ($peticiones as $p)
							@if(Auth::user()->tipo == 'admin' or Auth::user()->tipo == 'superuser' or $p->id_user_asignado == Auth::user()->id)
							<tr>
								<td class="text-center">
									<a href="{{ route('peticion.edit', $p->id) }}">{{ $p->id }}</a>
								</td>
								<td class="text-center">{{ $p->nombre }}</td>
								<td class="text-center">{{ $p->tipo}}</td>
								<td class="text-center">
									@php
										$date1 = date_create($p->created_at);
				                        $interval = date_diff($p->created_at, $hoy); 
				                        $dias = $interval->format('%a');
          								$date = date_format($date1, 'd/m/Y');
									@endphp
									{{ $date }}
									<br>
									@if($p->respuesta == null or $p->respuesta == '')
					                    @if($p->tipo == 'Petición')
					                        @if ($dias > ($dias_peticion - 3)) 
					                        	<span class="label" style="color: #dd4b39;">Riesgo de expiración</span>
					                        @endif
					                    @endif
					                    @if($p->tipo == 'Queja')
					                        @if ($dias > ($dias_queja - 3)) 
					                        	<span class="label" style="color: #dd4b39;">Riesgo de expiración</span>
					                        @endif
					                    @endif
					                    @if($p->tipo == 'Reclamo')
					                        @if ($dias > ($dias_reclamo - 3)) 
					                        	<span class="label" style="color: #dd4b39;">Riesgo de expiración</span>
					                        @endif
					                    @endif
					                    @if($p->tipo == 'Sugerencia')
					                        @if ($dias > ($dias_sugerencia - 3)) 
					                        	<span class="label" style="color: #dd4b39;">Riesgo de expiración</span>
					                        @endif
					                    @endif
					                @endif
								</td>
								<td class="text-center">
									@if($p->estadoIN->estado == 'Nueva')
										<span class="label bg-orange">
											{{ $p->estadoIN->estado }}		
										</span>
									@elseif($p->estadoIN->estado == 'Expirada')
										<span class="label bg-maroon">
											{{ $p->estadoIN->estado }}		
										</span>
									@elseif($p->estadoIN->estado == 'Con respuesta')
										<span class="label bg-navy">
											{{ $p->estadoIN->estado }}		
										</span>
									@endif									
								</td>
								<td class="text-center">
									<a class="btn btn-primary" href="{{ route('peticion.edit', $p->id) }}">Detalles</a>
								</td>
							</tr>
							@endif
						@endforeach
					</table>
					
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
      <div class="col-md-12 text-center">
				{{ $peticiones->links() }}
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	{{-- <script src="{{ asset('/js/buscador.js') }}" type="text/javascript"></script> --}}
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
	</script>
@endsection