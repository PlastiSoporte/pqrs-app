@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    PQRS CCM
@endsection
@section('content')

<body class="hold-transition login-page" >

    <div id="app">   	

    	<div class="col-md-8 col-md-offset-2">
    		<br>
    			<div>
    					<img src="{{ asset('/img/logo.png') }}" alt="CCM-tv">
    			</div>
    		<br>

    		{{-- Alerta de acciones --}}
			@if (session('status'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ session('status') }}
				</div>
			@endif
				<div class="box box-warning">

					<div class="box-header with-border">
						<h3 class="box-title">Preguntas, Quejas, Reclamos, Sugerencias</h3>
						<hr>
        			<a href="{{url('peticion/verificacion')}} ">Estado de mi petición</a>
					</div>

        <div class="box-body">

        <form action="{{ route('peticion.store') }}" method="post">
        	{{ csrf_field() }}
        	<div class="col-md-6">

        		@php $campo = 'nombre' @endphp
				<div class="form-group @if($errors->has($campo)) has-error @endif">
					<label for="{{ $campo }}">Nombre</label>
					<input type="text" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el nombre" value="{{ old($campo) }}">
					@if ($errors->has($campo))
						@foreach ($errors->get($campo) as $error)
							<span class="help-block">{{ $error }}</span>
						@endforeach
					@endif
				</div>

			  @php $campo = 'tipo_documento' @endphp
	            <div class="form-group @if($errors->has($campo)) has-error @endif">
	              <label for="{{ $campo }}">Tipo de documento</label>
	              <select class="form-control select2" name="{{ $campo }}" id="{{ $campo }}">
	              	<option value="">Seleccionar..</option>
	                <option value="Tarjeta de identidad" @if(old($campo) == 'Tarjeta de identidad') selected="selected" @endif >Tarjeta de identidad</option>

						<option value="Cédula de Ciudadanía" @if(old($campo) == 'Cédula de Ciudadanía') selected="selected" @endif >Cédula de Ciudadanía</option>

						<option value="Registro Civil" @if(old($campo) == 'Registro Civil') selected="selected" @endif >Registro Civil</option>

						<option value="Cédula de Extranjería" @if(old($campo) == 'Cédula de Extranjería') selected="selected" @endif >Cédula de Extranjería</option>

						<option value="Pasaporte" @if(old($campo) == 'Pasaporte') selected="selected" @endif >Pasaporte</option>
		 				
						<option value="Nit" @if(old($campo) == 'Nit') selected="selected" @endif >Nit</option>
	              </select>
	              @if ($errors->has($campo))
	                @foreach ($errors->get($campo) as $error)
	                  <span class="help-block">{{ $error }}</span>
	                @endforeach
	              @endif
	            </div>


				 @php $campo = 'documento' @endphp
				<div class="form-group @if($errors->has($campo)) has-error @endif">
					<label for="{{ $campo }}">Documento</label>
					<input type="number" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el numero de documento" value="{{ old($campo) }}">
					@if ($errors->has($campo))
						@foreach ($errors->get($campo) as $error)
							<span class="help-block">{{ $error }}</span>
						@endforeach
					@endif
				</div>
        	</div>

        	<div class="col-md-6">

             @php $campo = 'telefono' @endphp
						<div class="form-group @if($errors->has($campo)) has-error @endif">
							<label for="{{ $campo }}">Numero de teléfono. <small>(este campo es opcional.)</small></label>
							<input type="number" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Numero de teléfono" value="{{ old($campo) }}">
							@if ($errors->has($campo))
								@foreach ($errors->get($campo) as $error)
									<span class="help-block">{{ $error }}</span>
								@endforeach
							@endif
						</div>

						 @php $campo = 'email' @endphp
						<div class="form-group @if($errors->has($campo)) has-error @endif">
							<label for="{{ $campo }}">Correo Electronico</label>
							<input type="email" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el el correo electronico" value="{{ old($campo) }}">
							@if ($errors->has($campo))
								@foreach ($errors->get($campo) as $error)
									<span class="help-block">{{ $error }}</span>
								@endforeach
							@endif
						</div>

					@php $campo = 'tipo' @endphp
					<div class="form-group @if($errors->has($campo)) has-error @endif">
					<label for="{{ $campo }}">Tipo petición</label>
					<select class="form-control select2" name="{{ $campo }}" id="{{ $campo }}">
						<option value="">Seleccionar..</option>
						<option value="Petición" @if(old($campo) == 'Petición') selected="selected" @endif >Petición</option>

						<option value="Queja" @if(old($campo) == 'Queja') selected="selected" @endif >Queja</option>

						<option value="Reclamo" @if(old($campo) == 'Reclamo') selected="selected" @endif >Reclamo</option>

						<option value="Sugerencia" @if(old($campo) == 'Sugerencia') selected="selected" @endif >Sugerencia</option>
					</select>
					@if ($errors->has($campo))
					@foreach ($errors->get($campo) as $error)
					<span class="help-block">{{ $error }}</span>
					@endforeach
					@endif
					</div>

        	</div>


			@php $campo = 'peticion' @endphp
			<div class="form-group @if($errors->has($campo)) has-error @endif">
				<textarea class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa la descripción de su petición"  cols="20" rows="10">{{ old($campo) }}</textarea>							
				@if ($errors->has($campo))
					@foreach ($errors->get($campo) as $error)
						<span class="help-block">{{ $error }}</span>
					@endforeach
				@endif
			</div>

						<div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                </div><!-- /.col -->
            </div>

        </form>
    	</div>
    	{{-- /box-body --}}
    </div>
		{{-- /box --}}
   </div>
  {{-- /col --}}
  </div>
  {{-- /div-app --}}

    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
