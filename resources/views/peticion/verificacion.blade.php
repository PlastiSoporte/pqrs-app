@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    PQRS CCM
@endsection
@section('content')

<body class="hold-transition login-page" >

    <div id="app">   	

    	<div class="col-md-6 col-md-offset-3">
    		<br>
    			<div>
    					<img src="{{ asset('/img/logo.png') }}" alt="CCM-tv">
    			</div>
    		<br>

    		{{-- Alerta de acciones --}}
			@if (session('status'))
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ session('status') }}
				</div>
			@endif
				<div class="box box-warning">

					<div class="box-header with-border">
						<h3 class="box-title">Estado de petición</h3>
						<hr>
        				<a href="{{ url('/peticion/create') }}">Hacer una petición</a>
					</div>

        <div class="box-body">

        <form action="{{ route('peticion.validacion') }}" method="post">
        	{{ csrf_field() }}

        		@php $campo = 'radical' @endphp
				<div class="form-group">
					<label for="{{ $campo }}">Numero de radical</label>
					<input type="number" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el numero de radical" value="{{ old($campo) }}" required>
				</div>

				 @php $campo = 'email' @endphp
				<div class="form-group">
					<label for="{{ $campo }}">Correo electronico</label>
					<input type="email" class="form-control" name="{{ $campo }}" id="{{ $campo }}" placeholder="Ingresa el el correo electronico" value="{{ old($campo) }}" required>
				</div>

				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Siguiente</button>
					</div><!-- /.col -->
				</div>
        	</form>
    	</div>
    	{{-- /box-body --}}
    </div>
		{{-- /box --}}
   </div>
  {{-- /col --}}
  </div>
  {{-- /div-app --}}

    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
