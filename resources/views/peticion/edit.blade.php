@extends('template.app')

@section('htmlheader_title', 'Petición: '.$p->id)

@section('stylesheet')
	<link href="{{ asset('/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
@endsection

@section('contentheader_title', 'Peticiones.')

@section('contentheader_description', $p->tipo)

@section('breadcrumb')
	<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Inicio</a></li>
	<li><a href="{{ url('peticion') }}">Peticiones</a></li>
	<li class="active">Peticion {{$p->id}}</li>
@endsection

@section('main-content')
	
	<form method="POST" action="{{ route('peticion.update', $p->id) }}">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <input type="hidden" name="crearcomentario" value="{{ route('comentario.store') }}">
    <div class="row">
      <div class="col-md-6">

        <div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Información de la petición</h3>
          </div>

          <div class="box-body">

              <div class="form-group">
                <label>Numero de remisión:</label>
                <input type="text" class="form-control" value="{{ $p->id }}" disabled>
              </div>

              <div class="form-group">
                  <label>Fecha de petición: </label>
                  @php
                    $date = date_create($p->created_at);
                    $date = date_format($date, 'd/m/Y');
                  @endphp
                  <input type="text" class="form-control" value="{{ $date }}" disabled>
              </div>

               <div class="form-group">
                    <label>Estado de la petición: </label>
                    @if($p->estadoIN->estado == 'Nueva')
                      <span class="label bg-orange">
                        {{ $p->estadoIN->estado }}    
                      </span>
                    @elseif($p->estadoIN->estado == 'Expirada')
                      <span class="label bg-maroon">
                        {{ $p->estadoIN->estado }}    
                      </span>
                    @elseif($p->estadoIN->estado == 'Con respuesta')
                      <span class="label bg-navy">
                        {{ $p->estadoIN->estado }}    
                      </span>
                    @endif  
                </div>  

          </div>
          <!-- /.box-body -->
        </div>
        <!-- .box -->


        <div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Información de usuario</h3>
          </div>

          <div class="box-body">

            @php $campo = 'nombre'; @endphp     
            <div class="form-group">
              <label for="{{ $campo }}">Nombre</label>
              <input type="text" class="form-control" value="{{ $p->$campo }}" disabled>
            </div>

            @php $campo = 'tipo_documento'; @endphp
            <div class="form-group">
              <label for="{{ $campo }}">Tipo de documento</label>
              <select class="form-control select2" disabled>
                <option value="">Seleccionar..</option>
                <option value="Tarjeta de identidad" @if($p->$campo == 'Tarjeta de identidad') selected="selected" @endif >Tarjeta de identidad</option>

                <option value="Cédula de Ciudadanía" @if($p->$campo == 'Cédula de Ciudadanía') selected="selected" @endif >Cédula de Ciudadanía</option>

                <option value="Registro Civil" @if($p->$campo == 'Registro Civil') selected="selected" @endif >Registro Civil</option>

                <option value="Cédula de Extranjería" @if($p->$campo == 'Cédula de Extranjería') selected="selected" @endif >Cédula de Extranjería</option>

                <option value="Pasaporte" @if($p->$campo == 'Pasaporte') selected="selected" @endif >Pasaporte</option>

                <option value="Nit" @if($p->$campo == 'Nit') selected="selected" @endif >Nit</option>
              </select>
            </div>

            @php $campo = 'documento'; @endphp
            <div class="form-group">
              <label>Numero de documento</label>
              <input type="number" class="form-control" value="{{$p->$campo}}" disabled>
            </div>

            @php $campo = 'telefono'; @endphp
            <div class="form-group">
              <label>Numero de telefono</label>
              <input type="number" class="form-control" value="{{$p->$campo}}" disabled>
            </div>

            @php $campo = 'email'; @endphp
            <div class="form-group">
              <label for="{{ $campo }}">Email</label>
              <input type="text" class="form-control" value="{{ $p->$campo }}" disabled>
            </div>

            @php $campo = 'peticion'; @endphp
            <div class="form-group">
              <label for="{{ $campo }}">Descripción</label>
              <textarea class="form-control" cols="20" rows="10" readonly>{{ $p->$campo }}</textarea>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->

      <div class="col-md-6">
        <div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Respuesta</h3>
          </div>

          <div class="box-body">

            @if(Auth::user()->tipo == 'admin' or Auth::user()->tipo == 'superuser')
                @php $campo = 'id_user_asignado'; @endphp
                <div class="form-group">
                  <label for="{{ $campo }}">Asignar petición</label>
                  <select class="form-control select2" name="{{$campo}}">
                    <option value="0">Seleccionar..</option>
                    @foreach($users as $u)
                      @if($u->tipo <> 'superuser')
                        <option value="{{$u->id}}" @if(old($campo, $p->$campo) == $u->id) selected="selected" @endif>{{$u->name}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              @endif

            @if(Auth::user()->tipo == 'user')
              <input type="hidden" name="id_user_asignado" value="{{$p->id_user_asignado}}">
            @endif
            
            @php $campo = 'respuesta'; @endphp
            <div class="form-group @if($errors->has($campo)) has-error @endif">
              <label for="{{ $campo }}">Respuesta a la petición</label>
              <textarea class="form-control" cols="20" rows="10" maxlength="500" name="{{$campo}}" placeholder="Ingresa la respuesta a la petición">{{ old($campo, $p->$campo) }}</textarea>
              @if ($errors->has($campo))
                @foreach ($errors->get($campo) as $error)
                  <span class="help-block">{{ $error }}</span>
                @endforeach
              @endif
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- .box -->

        <div class="box box-primary">
          
          <div class="box-header with-border">
            <h3 class="box-title">Comentarios</h3>
          </div>

          <div class="box-body">
            @php $campo = 'comentario'; @endphp
            <div class="form-group" id="div-{{$campo}}">
              <label for="{{ $campo }}">Hacer un comentario</label>
              <textarea class="form-control" placeholder="Comentario.." id="{{$campo}}"></textarea>
              <input type="hidden" id="nombre-user-{{$campo}}" value="{{ Auth::user()->name}}">
              <input type="hidden" id="tipo-{{$campo}}" value="user">
              <input type="hidden" id="id_peticion" value="{{ $p->id }}">
              <span class="help-block" id="span-{{$campo}}"></span>
            </div>
            <a class="btn btn-info" id="añadir-comentario">Comentar</a>
            <hr>

            <div class="box-footer box-comments" id="lista-comentarios">

               @foreach($comentarios as $c)
                @php
                  if($c->tipo_user == 'cliente'){
                    $foto = asset('/img/user.png');
                  }else{
                    $foto = asset('/img/logo.png'); 
                  }
                  $date = date_create($c->created_at);
                  $date = date_format($date, 'd/m/Y');
                @endphp
                <div class="box-comment">
                  <!-- User image -->
                  <img class="img-circle img-sm" src="{{ $foto }}" alt="User Image">
                  <div class="comment-text">
                    <span class="username">
                    {{ $c->nombre }}
                    <span class="text-muted pull-right">{{ $date }}</span>
                    </span>
                    {{ $c->comentario }}
                  </div>
                </div>
                @endforeach
            </div>

                          {{-- 
            @foreach($users as $u)
              <option value="{{$u->id}}" @if(old($campo) == $u->id) selected="selected" @endif>{{$u->name}}</option>
            @endforeach --}}
          </div>
          <!-- /.box-body -->
        </div>
        <!-- .box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-body text-center">
            <a href="{{ URL::previous() }}" role="button" class="btn btn-default">Cancelar</a>
            <button type="submit" class="btn btn-info">Guardar</button>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection

@section('scripts')
	<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
  <script src="{{ asset('/js/crearcomentario.js') }}"></script>
	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
		]) !!};
		$(function () {
			//Initialize Select2 Elements
			$('.select2').select2();

      //iCheck for checkbox and radio inputs
        $('input[type="radio"].flat-orange').iCheck({
          radioClass : 'iradio_flat-orange'
        })
		})
	</script>
@endsection