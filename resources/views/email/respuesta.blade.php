<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Llamado de emergencia</title>
    <style>
        body {
            background: #d2d6de;
            font-size: 17px;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            font-family: sans-serif;
        }
        .peticion {
            width: 80%;
            background: #fff;
            padding: 10px;
            border-radius: 2px;
            margin-left: 10%;
            margin-top: 5%;
            color: inherit;
        }
        .boton{
            text-decoration: none;
            color: #fff !important;
            padding: 7px;
            background: #337ab7;
            margin-bottom: 6px;
            border-color: #2e6da4;
        }
        .boton:hover{
            background: #2e6da4;   
        }
    </style>
</head>
<body>
    <div>
        <img src="{{ asset('/img/logo.png') }}" alt="CCM tv">
    </div>
    <div class="peticion">
        <h2>{{$peticion->nombre}}</h2><hr>
        <p>tipo de petición: {{$peticion->tipo}}</p>
        <p>Numero de remición: {{$peticion->id}}</p>
        <h3>PQRS CCM Tv</h3>
        <p>{{$respuesta}}</p>
        <hr><br>
        <a class="boton" href="http://server8.plastimedia.com/pqrs-app/public/peticion/verificacion" target="_blank">Ver estado de peticion</a>
         <p>Mensaje generado automatica mente. no responder o reenviar. <br>
            Copyright &copy; 2018 Canal Ccm - Corporacion PROACODEMA. Todos los derechos reservados
        </p>
    </div>
</body>
</html>