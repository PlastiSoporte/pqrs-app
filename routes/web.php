<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rutas para los usuarios
Route::get('peticion/create', 'PeticionController@create')->name('peticion.create');
Route::post('peticion', 'PeticionController@store')->name('peticion.store');
Route::get('peticion/verificacion', 'PeticionController@varificacion')->name('peticion.varificacion');
Route::post('peticion/validacion', 'PeticionController@validacion')->name('peticion.validacion');
Route::post('comentariocliente', 'ComentariosController@storecliente')->name('comentario.cliente');

Route::group(['middleware' => 'auth'], function () {

	// Rutas para los comentarios
	//Route::get('comentario', 'ComentariosController@index')->name('comentario.index');
	//Route::get('comentario/create', 'ComentariosController@create')->name('comentario.create');
	//Route::get('comentario/{id}/edit', 'ComentariosController@edit')->name('comentario.edit');
	Route::post('comentario', 'ComentariosController@store')->name('comentario.store');
	Route::put('comentario/{id}', 'ComentariosController@update')->name('comentario.update');

	// Rutas para editar perfil propio
	Route::get('perfil/{id}/edit', 'PerfilController@edit')->name('perfil.edit');
	Route::put('perfil/{id}', 'PerfilController@update')->name('perfil.update');
	Route::post('perfil', 'PerfilController@updatepass')->name('perfil.updatepass');
	// Route::put('users/{id}', 'UsersController@update')->name('perfil.update');

	// Rutas para las peticiones
	Route::get('peticion', 'PeticionController@index')->name('peticion.index');
	//Route::get('peticion/create', 'PeticionController@create')->name('peticion.create');
	Route::get('peticion/{id}/edit', 'PeticionController@edit')->name('peticion.edit');
	//Route::post('peticion', 'PeticionController@store')->name('peticion.store');
	Route::put('peticion/{id}', 'PeticionController@update')->name('peticion.update');
	
	Route::group(['middleware' => 'user'], function () {
		// Rutas para los usuarios
		Route::get('users', 'UsersController@index')->name('users.index');
		Route::get('users/create', 'UsersController@create')->name('users.create');
		Route::get('users/{id}/edit', 'UsersController@edit')->name('users.edit');
		Route::post('users', 'UsersController@store')->name('users.store');
		Route::put('users/{id}', 'UsersController@update')->name('users.update');

		// Rutas para la parametrizacion
		Route::get('parametrizacion', 'ParametrizacionController@index')->name('parametrizacion.index');
		//Route::get('parametrizacion/create', 'ParametrizacionController@create')->name('parametrizacion.create');
		Route::get('parametrizacion/{id}/edit', 'ParametrizacionController@edit')->name('parametrizacion.edit');
		//Route::post('parametrizacion', 'ParametrizacionController@store')->name('parametrizacion.store');
		Route::put('parametrizacion/{id}', 'ParametrizacionController@update')->name('parametrizacion.update');
	});



	

	Route::get('register', 'Auth\RegisterController@showRegistrationForm');
	Route::post('register', 'Auth\RegisterController@register');

	Route::get('/', function () {
		return view('home', ['activo' => 'home']); 
	});

   //    Route::get('/link1', function ()    {
	//        // Uses Auth Middleware
	//    });

	//Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
	#adminlte_routes
}); 
