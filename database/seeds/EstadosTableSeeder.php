<?php

use Illuminate\Database\Seeder;

class EstadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // *********************************
				// ESTADOS DE LAS PETICIONES
				// *********************************
    	  //estados internos*******
				DB::table('estados')->insert([
					'estado'=> 'Nueva',
				]);
				DB::table('estados')->insert([
					'estado'=> 'Expirada',
				]);				
				//estados externos*******
				DB::table('estados')->insert([
					'estado'=> 'Pendiente',
				]);
				DB::table('estados')->insert([
					'estado'=> 'Con respuesta',
				]);
    }
}
