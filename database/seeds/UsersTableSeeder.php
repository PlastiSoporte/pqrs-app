<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		// *********************************
		// USUARIOS ADMINISTRADORES
		// *********************************
		DB::table('users')->insert([
			'name' => 'Edwin Galeano',
			'email' => 'desarrollo@plastimedia.com',
			'password' => bcrypt('plasti249'),
			'tipo' => 'superuser',
		]);
		DB::table('users')->insert([
			'name' => 'Stiven Montoya',
			'email' => 'soporte@plastimedia.com',
			'password' => bcrypt('plasti249'),
			'tipo' => 'superuser',
		]);
	}
}
