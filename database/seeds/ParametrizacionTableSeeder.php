<?php

use Illuminate\Database\Seeder;

class ParametrizacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // *********************************
				// PARAMETRIZACION DE DIAS
				// *********************************
				DB::table('parametrizacion')->insert([
					'dias_peticion'=> 10,
					'dias_queja'=> 10,
					'dias_reclamo'=> 10,
					'dias_sugerencia'=> 10,
				]);
    }
}
