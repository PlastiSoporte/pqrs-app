<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150);
            $table->enum('tipo_user', ['user', 'cliente']);
            $table->text('comentario');
            $table->integer('id_peticion')->unsigned();

            //relaciones***
            $table->foreign('id_peticion')->references('id')->on('peticiones');
            
            $table->rememberToken();
            $table->timestamps();

                        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
