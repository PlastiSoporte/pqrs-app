<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaParametrizacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametrizacion', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('dias_peticion');
            $table->integer('dias_queja');
            $table->integer('dias_reclamo');
            $table->integer('dias_sugerencia');
            
            $table->rememberToken();
            $table->timestamps();

                        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametrizacion');
    }
}

