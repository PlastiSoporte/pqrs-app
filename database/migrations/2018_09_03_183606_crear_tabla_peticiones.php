<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPeticiones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peticiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150);
            $table->string('tipo_documento');
            $table->string('documento');
            $table->string('email', 100);
            $table->string('telefono')->nullable();

            $table->enum('tipo', ['Petición', 'Queja', 'Reclamo', 'Sugerencia']);
            $table->text('peticion');
            $table->text('respuesta')->nullable();
            
            $table->integer('id_user_asignado')->unsigned();
            $table->integer('id_estado_interno')->unsigned();
            $table->integer('id_estado_externo')->unsigned();

            //relaciones***
            $table->foreign('id_user_asignado')->references('id')->on('users');
            $table->foreign('id_estado_interno')->references('id')->on('estados'); 
            $table->foreign('id_estado_externo')->references('id')->on('estados');

            $table->rememberToken();
            $table->timestamps();

                        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peticiones');
    }
}
